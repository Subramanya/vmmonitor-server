from django.shortcuts import render
from django_socketio import broadcast, broadcast_channel, NoSocket


def connect(request):
    print "connection request"
    template = 'socket.html'
    context = {"output": 'it worked??'}
    if request.method == "POST":
        print request.POST["message"]
        context = {"output": 'it works !!!'}
        connecttion = request.POST["connect"]
        data = {"action": "system", "message": "message"}
        try:
            broadcast_channel(data, channel="connect")
        except NoSocket, e:
            context["message"] = e
        else:
            context["message"] = "Message sent"
    return render(request, template, context)