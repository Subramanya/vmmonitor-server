from django.views.generic.edit import CreateView, DeleteView, UpdateView
from django.views.generic import DetailView, ListView
from django.core.urlresolvers import reverse_lazy
from .models import Alert


class AlertList(ListView):
    model = Alert
    template_name = 'alert_list.html'
    context_object_name = 'object_list'

    def get_queryset(self):
        return Alert.objects.all()


class AlertDetail(DetailView):
    model = Alert
    template_name = 'alert_detail.html'


class AlertCreate(CreateView):
    model = Alert
    template_name = 'alert_create.html'
    fields = ['name', 'command', 'time', 'servers', 'message', 'equality', 'threshold']

    def get_success_url(self):
        return reverse_lazy('alert-list')


class AlertDelete(DeleteView):
    model = Alert
    template_name = 'alert_delete.html'
    success_url = reverse_lazy('alert-list')


class AlertUpdate(UpdateView):
    model = Alert
    template_name = 'alert_update.html'

    def get_context_data(self, **kwargs):
        context = super(AlertUpdate, self).get_context_data(**kwargs)
        context['action'] = reverse_lazy('alert-edit', kwargs={'pk': self.get_object().id})
        return context
