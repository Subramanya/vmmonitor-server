from django.db import models
from django.core.urlresolvers import reverse
from django.forms import ModelForm
from apps.servers.models import Server


class Alert(models.Model):
    EQUALITY_CHOICES = (
        ('S', 'Smaller than <'),
        ('E', 'Equals to =='),
        ('G', 'Greater than >'),
    )
    name = models.CharField(max_length=100)
    command = models.CharField(max_length=255)
    equality = models.CharField(max_length=1, choices=EQUALITY_CHOICES)
    threshold = models.IntegerField()
    time = models.IntegerField()
    message = models.CharField(max_length=150)
    servers = models.ManyToManyField(Server)

    def get_absolute_url(self):
        return reverse('alert-detail', kwargs={'pk': self.pk})

    def __str__(self):
        return self.name


class AlertFrom(ModelForm):
    class Meta:
        model = Alert