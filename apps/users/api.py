from tastypie.authorization import Authorization
from tastypie.resources import ModelResource
from .models import ServerUser
from django.contrib.auth.models import User


class ServerUserResources(ModelResource):
    class Meta:
        queryset = User.objects.all()
        resource_name = 'user'
        authorization = Authorization()