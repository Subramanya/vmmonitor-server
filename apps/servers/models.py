from django.db import models
from django.core.urlresolvers import reverse
from django.forms import ModelForm
from apps.users.models import ServerUser


class Server(models.Model):
    ip = models.CharField(max_length=15)
    location = models.CharField(max_length=100)
    name = models.CharField(max_length=100)
    users = models.ManyToManyField(ServerUser)

    def get_absolute_url(self):
        return reverse('server-detail', kwargs={'pk': self.pk})

    def __str__(self):
        return self.ip


class ServerForm(ModelForm):
    class Meta:
        model = Server