from django.views.generic.edit import CreateView, DeleteView, UpdateView
from django.views.generic import ListView
from django.views.generic import DetailView
from django.core.urlresolvers import reverse_lazy
from .models import Server


class ServerDetail(DetailView):
    model = Server
    template_name = 'server_detail.html'


class ServerList(ListView):
    model = Server
    template_name = 'server_list.html'
    context_object_name = 'object_list'

    def get_queryset(self):
        return Server.objects.all()[:10]


class ServerCreate(CreateView):
    model = Server
    template_name = 'server_create_form.html'
    fields = ['ip', 'name', 'location']

    def get_success_url(self):
        return reverse_lazy('server-list')


class ServerDelete(DeleteView):
    model = Server
    success_url = reverse_lazy('server-list')
    template_name = 'server_confirm_delete.html'


class ServerUpdate(UpdateView):
    model = Server
    template_name = 'server_update_form.html'
    success_url = reverse_lazy('server-list')

    def get_context_data(self, **kwargs):
        context = super(ServerUpdate, self).get_context_data(**kwargs)
        context['action'] = reverse_lazy('servers-edit', kwargs={'pk': self.get_object().id})
        return context