from tastypie.authorization import Authorization
from tastypie.resources import ModelResource, ALL
from .models import Statistic


class StatisticResource(ModelResource):
    class Meta:
        queryset = Statistic.objects.order_by("-id").all()
        resource_name = 'statistics'
        list_allowed_methods = ['get']
        authorization = Authorization()
        filtering = {
            'server_id': ALL,
        }
        ordering = {
            'server_id': ALL,
        }


class SpecificStatisticResource(ModelResource):
    class Meta:
        queryset = Statistic.objects.all()
        resource_name = 'list-statistics'
        list_allowed_methods = ['get']
        authorization = Authorization()

    def dehydrate(self, bundle):
        print 'dehydrate'
        #data = bundle.data['server_id']
        #Statistic.objects.get(server_id=data)
        return bundle

'''
    def hydrate(self, bundle):
        script_id = bundle.data['script_id']
        run_script = Script.objects.get(pk=script_id)
        print run_script.file.name
        server_id = bundle.data['server_id']
        status = bundle.data['status']
        #subprocess_output = subprocess.check_output('/home/rodrigo/sourcecode/vmmonitor-server/scripts/' + run_script.file.name)
        print run_script.file.name
        subprocess_output = subprocess.check_output('C:/Users/rodrigo/PycharmProjects/VMMonitor/userfiles/' + run_script.file.name)
        print subprocess_output
        bundle.data['status'] = subprocess_output
        print 'Script: %s | Server: %s | Status: %s' % (script_id, server_id, status)
        return bundle
'''