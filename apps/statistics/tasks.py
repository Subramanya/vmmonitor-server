import datetime
import celery

@celery.decorators.periodic_task(run_every=datetime.timedelta(seconds=5))
def myfunc():
    print 'periodic_task'